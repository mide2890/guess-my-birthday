from random import randint

birth_month = randint(1, 12)
birth_year = randint (1924, 2004)

range_guesses = range(5)

name = input("Hi! What is your name?\n")

for num in range_guesses:

    birth_month = randint(1, 12)
    birth_year = randint (1924, 2004)

    print(f"Were you born in: {birth_month} / {birth_year} ? \n")
    guess = input("Yes or no?\n")

    if guess == "yes":
        print("I knew it!")
        break

    elif guess == "no" and num in range_guesses == 5:
        print("Drat! Lemme try again!")

    else: print("I have other things to do. Good bye.")
